#include<stdio.h>
#include<string>
#include<malloc.h>
#define DIM 100

struct Angajat {
	int ID;
	char* nume;
	char* adresa;
	float salariu;
	int Entry;
};

struct NodLD {
	NodLD* prev;
	Angajat ang;
	NodLD* next;
};

struct LD {
	NodLD* prim, *ult;
};

struct ABE {
	Angajat ang;
	ABE *st;
	ABE *dr;

};

LD AdaugaLD(LD ld, Angajat ang)
{
	
	NodLD* nou = (NodLD*)malloc(sizeof(NodLD));

	nou->ang.ID = ang.ID;
	nou->ang.nume = ang.nume;
	nou->ang.adresa = ang.adresa;
	nou->ang.salariu = ang.salariu;
	nou->ang.Entry = ang.Entry;

	nou->prev = ld.ult;
	nou->next = NULL;
	if (ld.prim==NULL)
	{
		ld.prim = nou;
		ld.ult = nou;
		return ld;
	}
	ld.ult->next = nou;
	ld.ult = nou;
	return ld;
}

void AfisareLD(LD ld) 
{
	NodLD* temp=ld.prim;
	while (temp)
	{
		printf("\nAng are ID %d\n", temp->ang.ID);
		temp = temp->next;
	}
	temp = ld.ult;
	while (temp)
	{
		printf("\nAng are ID %d\n", temp->ang.ID);
		temp = temp->prev;
	}
	
}

LD StergeNod(LD ld) {
	NodLD* temp = ld.prim;
	while (temp!=NULL)

	{
		if (temp->ang.Entry == 0)
		{
			if (temp->prev == NULL)
			{
				temp->next->prev = NULL;
				ld.prim = temp->next;
				free(temp->ang.nume);
				free(temp->ang.adresa);
			}
			else if(temp->next==NULL)
			{
				temp->prev->next = NULL;
				ld.ult = temp->prev;
				free(temp->ang.nume);
				free(temp->ang.adresa);

			}
			else
			{
				temp->prev->next = temp->next;
				temp->next->prev = temp->prev;
				free(temp->ang.nume);
				free(temp->ang.adresa);
			}
		
			temp = temp->next;
		
		}
		return ld;
	}
}

ABE* InsertABE(ABE* root, Angajat ang) {
	if (root)
	{
		if (root->ang.ID > ang.ID)
		{
			root->st = InsertABE(root->st, ang);
		}
		else
		{
			root->dr = InsertABE(root->dr, ang);
		}
	}
	else
	{
		ABE* nou = (ABE*)malloc(sizeof(ABE));
		nou->ang.ID = ang.ID;
		nou->ang.adresa = (char*)malloc(sizeof(char)*(strlen(ang.adresa)+1));
		strcpy(nou->ang.adresa, ang.adresa);
		nou->ang.nume = (char*)malloc(sizeof(char)*(strlen(ang.nume) + 1));
		strcpy(nou->ang.nume, ang.nume);
		nou->ang.salariu = ang.salariu;
		nou->ang.Entry = ang.Entry;
		nou->st = NULL;
		nou->dr = NULL;
		root = nou;
	}
	return root;
}

ABE* MakeABE(ABE* root, LD ld)
{
	NodLD* temp = ld.prim;
	while (temp)
	{
		if (temp->ang.salariu > 1200)
		{
			root = InsertABE(root, temp->ang);
		}
		temp = temp->next;
		
	}
	return root;
}

void AfisareABE(ABE* root)
{
	if (root)
	{
		AfisareABE(root->st);
		printf(" \n%d\n", root->ang.ID);
		AfisareABE(root->dr);
	}
}

void splitListaDubla(LD ld, LD* ld1, LD* ld2) {

	NodLD* temp = ld.prim;
	while (temp) {
		if (temp->ang.Entry == 0) {
			AdaugaLD(*ld1, temp->ang);
		}
		else {
			AdaugaLD(*ld2, temp->ang);
			
		}
		temp = temp->next;
	}
}
void main()
{
	LD ld1;
	ld1.prim = NULL;
	ld1.ult = NULL;
	LD ld;
	ld.prim = NULL;
	ld.ult = NULL;
	FILE* f = fopen("Ang.txt", "r");
	char buffer[DIM], *p, sepList[] = ",";
	Angajat ang;
	while (fgets(buffer, sizeof(buffer), f))
	{
		p = strtok(buffer, sepList);
		ang.ID = atoi(p);
		p = strtok(NULL, sepList);
		ang.nume = (char*)malloc(sizeof(char)*(strlen(p) + 1));
		strcpy(ang.nume, p);
		p = strtok(NULL, sepList);
		ang.adresa = (char*)malloc(sizeof(char)*(strlen(p) + 1));
		strcpy(ang.adresa, p);
		p = strtok(NULL, sepList);
		ang.salariu = atof(p);
		p = strtok(NULL, sepList);
		ang.Entry = atoi(p);
		ld = AdaugaLD(ld, ang);
		
	}
	AfisareLD(ld);
	ld = StergeNod(ld);
	printf("\n *****************\n");
	AfisareLD(ld);
	ABE *root = NULL;
	root = MakeABE(root, ld);
	AfisareABE(root);
	printf("\n**************\n");
	LD ld2;
	ld2.prim = NULL;
	ld2.ult = NULL;
	splitListaDubla(ld, &ld1, &ld2);
	AfisareLD(ld1);
	AfisareLD(ld2);
}