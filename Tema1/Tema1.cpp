#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include<stdlib.h>


struct BYOD
{
	char* name;		
	char** features;
	int nrFeatures;
	int release;
	int nrPieces; 
};

void initBYOD(BYOD* byod, 
		const char* name,
		const char ** features,
		const int nrFeatures,
		const int release,
		const int nrPieces)
{
	byod->name = (char*)malloc((strlen(name) +1) * sizeof(char));
	strcpy(byod->name, name);

	byod->nrFeatures = nrFeatures;
	byod->features = (char**)malloc((nrFeatures) * sizeof(char*));
	for (int i = 0; i < nrFeatures; i++)
	{
		byod->features[i] = (char*)malloc((strlen(features[i])+1) * sizeof(char));
		strcpy(byod->features[i], features[i]);
	}

	byod->release = release;
	byod->nrPieces = nrFeatures;
}

void freeBYOD(BYOD* byod)
{
	free(byod->name);
	for (int i = 0; i < byod->nrFeatures; i++)
	{
		free(byod->features[i]);
	}
	free(byod->features);
}

void displayBYOD(BYOD* byod)
{
	printf("Gadgetul %s are urmatoarele %i functii:",byod->name,byod->nrFeatures);
	for (int i = 0; i < byod->nrFeatures; i++)	
	{
		if (i > 0) printf(",");
		printf(" %s", byod->features[i]);
	}
	printf(" , a fost introdus in anul %i si are deja %i bucati vandute.\n", byod->release, byod->nrPieces);
}


struct Nod
{
	BYOD* info; 
	Nod* urm;
};



int stackPush(Nod** stack, BYOD *byod)
{
	Nod* n = (Nod*)malloc(sizeof(Nod));
	n->info = byod;
	n->urm = (*stack);
	*stack = n;
	return 0;
}
int stackPop(Nod** stack, BYOD **b)
{
	if (!*stack) return -1; 
	*b = (*stack)->info;
	Nod* n = *stack; 
	*stack = (*stack)->urm;	
	free(n);
	return 0;
}
int stackPeek(Nod** stack, BYOD** b)
{
	if (!*stack) return -1; 
	*b = (*stack)->info;
	return 0;
}


struct Queue
{
	Nod* head;
	Nod* tail;
};

int queueEnqueue(Queue* queue, BYOD* byod)
{
	Nod* n = (Nod*)malloc(sizeof(Nod));
	n->info = byod;
	n->urm = NULL;
	if (queue->tail)
	{
		queue->tail->urm = n;
		queue->tail = n;
	}
	else
	{
		queue->tail = queue->head = n;
	}
	return 0;
}
int queueDequeue(Queue* queue, BYOD** b)
{
	if (!queue->head) return -1; 
	*b = (queue->head)->info;
	Nod* n = queue->head; 
	queue->head = (queue->head)->urm;
	if (!queue->head) queue->tail = NULL;
	free(n);
	return 0;
}
int queuePeek(Queue* queue, BYOD** b)
{
	if (!queue->head) return -1; 
	*b = (queue->head)->info;	
	return 0;
}




int filtrare1(Nod* head, int minSales, BYOD*** array, int* sizeOfArray)
{


	Queue q; q.head = q.tail = NULL;
	int c = 0;
	while (head)
	{
		if (head->info->nrPieces >= minSales) 
		{
			queueEnqueue(&q, head->info);
			c++;
		}
		head = head->urm;
	}


	BYOD** a = (BYOD**)malloc(sizeof(BYOD*) * c);
	for (int i = 0; i < c; i++)
	{
		queueDequeue(&q, a+i);
	}
	*sizeOfArray = c;
	*array = a;

	return 0;
}


int filtrare2(Queue *queue, int minVanzari, BYOD*** array, int* sizeOfArray)
{
		
	
	


	Queue noua; noua.head = noua.tail = NULL; 
	Queue q; q.head = q.tail = NULL; 
	int c = 0;

	BYOD* current;
	while (queueDequeue(queue,&current)>=0)
	{
		if (current->nrPieces >= minVanzari)
		{
			queueEnqueue(&q, current);
			c++;
		}
		else
		{
			queueEnqueue(&noua, current);
		}
	}

	BYOD** a = (BYOD**)malloc(sizeof(BYOD*) * c);
	for (int i = 0; i < c; i++)
	{
		queueDequeue(&q, a + i);
	}
	*sizeOfArray = c;
	*array = a;


	queue->head = noua.head;
	queue->tail = noua.tail;

	return 0;
}


int main()
{

	{ 
		BYOD g;
		const char* x[] = { "Economie baterie", "Contacte", "Applicatii" };
		initBYOD(&g, "Smartphone", x, 3, 2020, 100);
		displayBYOD(&g);
		freeBYOD(&g);

		BYOD* d = (BYOD*)malloc(sizeof(BYOD));
		const char* y[] = { "aspira", "calca", "spala vase" };
		initBYOD(d, "Robotul Minune", y, 3, 2020, 100);
		displayBYOD(d);
		freeBYOD(d);
		free(d);
	}

	Nod* stack = NULL; 

	Queue q; q.head = q.tail = NULL; 

	Queue q2; q2.head = q2.tail = NULL;


	BYOD* d;

	d = (BYOD*)malloc(sizeof(BYOD));
	const char* y1[] = { "aspira", "calca", "spala vase" };
	initBYOD(d, "1.Robotul Minune", y1, 3, 2020, 100);
	stackPush(&stack, d); 
	queueEnqueue(&q, d); 

	d = (BYOD*)malloc(sizeof(BYOD));
	const char* y2[] = { "aspira", "calca", "spala vase" };
	initBYOD(d, "2. Electro Minune", y1, 3, 2020, 100);
	stackPush(&stack, d); 

	queueEnqueue(&q, d); 
	queueEnqueue(&q, d); 
	BYOD d3;
	initBYOD(&d3, "3. SmartPhone", y1, 3, 2020, 100);
	queueEnqueue(&q, &d3); 
	queueEnqueue(&q, &d3); 

	
	BYOD* current = NULL;
	printf("\n\n PROCESARE COADA \n");
	while (queueDequeue(&q, &current)==0)
	{
		
		
		current->nrPieces += 10;
		displayBYOD(current);

		queueEnqueue(&q2, current);
	}

	
	printf("\n\n filtrare1 in vector \n");
	BYOD** dynamicArray;
	int sizeOfDynamicArray;

	filtrare1(stack, 115, &dynamicArray, &sizeOfDynamicArray);

	for (int i = 0; i < sizeOfDynamicArray; i++)
	{
		displayBYOD(dynamicArray[i]);
	}

	free(dynamicArray);

	printf("\n\n filtrare2 in vector \n");
		

	filtrare2(&q2, 115, &dynamicArray, &sizeOfDynamicArray);
	printf("\n\n ce e in vector\n");
	for (int i = 0; i < sizeOfDynamicArray; i++)
	{
		displayBYOD(dynamicArray[i]);
	}
	free(dynamicArray);

	printf("\n\n ce a ramas in coada \n");
	while (queueDequeue(&q2, &current) == 0)
	{
		displayBYOD(current);
	}


	
	printf("\n\n DEZALOCARE \n");
	while (stackPop(&stack, &current) == 0)
	{
		displayBYOD(current);

		freeBYOD(current);
		free(current);
	}

	freeBYOD(&d3);


	return 0;
}
