#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string>


struct Tara {
	int id;
	char* nume;
	int nrLoc;
};

struct Nod {
	Tara info;
	Nod*st;
	Nod* dr;
};

Tara citesteDinFisier(FILE * fis) {
	Tara t;
	fscanf(fis, "%d", &t.id);
	fscanf(fis, "%d", &t.nrLoc);
	char aux[20];
	fscanf(fis, "%s", aux);
	t.nume = (char*)malloc(sizeof(char)*(strlen(aux) + 1));
	strcpy(t.nume, aux);
	return t;
}

int maxim(int a, int b) {
	return ((a > b) ? a : b);
}

int inaltimeArbore(Nod* rad) {
	if (rad) {
		int Hst = inaltimeArbore(rad->st);
		int Hdr = inaltimeArbore(rad->dr);
		return maxim(Hst, Hdr) + 1;
	}
	else {
		return 0;
	}
}

int calculGradEchilibru(Nod* rad) {
	if (rad) {
		int Hst = inaltimeArbore(rad->st);
		int Hdr = inaltimeArbore(rad->dr);

		return Hst - Hdr;
	}
	else {
		return 0;
	}
}

Nod* inserareTaraInArbore(Nod* rad, Tara t) {
	if (rad) {
		if (rad->info.id < t.id) {
			rad->dr = inserareTaraInArbore(rad->dr, t);
		}
		else if (rad->info.id > t.id) {
			rad->st = inserareTaraInArbore(rad->st, t);
		}
		else {
			throw "Tara cu acest id exista deja";
		}
		return rad;
	}
	else {
		Nod* nou = (Nod*)malloc(sizeof(Nod));
		nou->info = t;
		nou->dr = NULL;
		nou->st = NULL;
		return nou;
	}
}


Nod* rotireStanga(Nod* rad) {
	if (rad) {
		Nod*temp = rad->dr;
		rad->dr = temp->st;
		temp->st = rad;
		return temp;
	}
	else {
		return rad;
	}
}

Nod* rotireDreapta(Nod* rad) {
	if (rad) {
		Nod* temp = rad->st;
		rad->st = temp->dr;
		temp->dr = rad;
		return temp;
	}
	else {
		return NULL;
	}
}


Nod* inserareTaraInArboreAVL(Nod* rad, Tara t) {
	if (rad) {
		if (rad->info.id < t.id) {
			rad->dr = inserareTaraInArbore(rad->dr, t);
		}
		else if (rad->info.id > t.id) {
			rad->st = inserareTaraInArbore(rad->st, t);
		}
		else {
			throw "Tara cu acest id exista deja";
		}
		int GE = calculGradEchilibru(rad);
		if (GE == -2) {
			//dezechilibru in dreapta
			if (calculGradEchilibru(rad->dr) == -1) {
				rad = rotireStanga(rad);
			}
			else {
				rad->dr = rotireDreapta(rad->dr);
				rad = rotireStanga(rad);
			}
		}
		else {
			if (GE == 2) {
				//dezchilibru in stanga
				if (calculGradEchilibru(rad->st) == -1) {
					rad->st = rotireStanga(rad->st);
				}
				rad = rotireDreapta(rad);
			}
		}

		return rad;
	}
	else {
		Nod* nou = (Nod*)malloc(sizeof(Nod));
		nou->info = t;
		nou->dr = NULL;
		nou->st = NULL;
		return nou;
	}
}

void afisareTara(Tara t) {

	printf("Tara %s cu id-ul %d are %d\n", t.nume, t.id, t.nrLoc);
}

void afisareInordine(Nod* rad) {
	if (rad) {
		afisareInordine(rad->st);
		afisareTara(rad->info);
		afisareInordine(rad->dr);
	}
}

Tara cautareTaraDupaId(Nod* rad, int id) {
	if (rad) {
		if (rad->info.id == id) {
			return rad->info;
		}
		else if (rad->info.id > id) {
			return cautareTaraDupaId(rad->st, id);
		}
		else {
			return cautareTaraDupaId(rad->dr, id);
		}
	}
	else {
		throw ;
	}
}



int contor = 0;
int cautareNrLocTari(Nod* rad, int nrloc) {


	if (rad) {
		if (rad->info.nrLoc > nrloc) {
			contor++;
		}

		cautareNrLocTari(rad->dr, nrloc);
		cautareNrLocTari(rad->st, nrloc);


		return contor;

	}

}

int StergeNod(Nod* &arb, int id)
{//3 noduri temp
	Nod *temp, *temp1, *temp2;
	// cautam
	temp1 = arb;
	while (temp1->info.id == id)
	{
		if (temp1->info.id > id)
			temp1 = temp1->st;

		else {
			printf("eroare");
		}

	}


	temp1 = temp1->dr;

	temp = temp1;
	//daca am frunza st

	if (temp1->dr == NULL) {
		//mutare inf din nodul din st, in nodul curent
		temp1->info = temp1->st->info;
		//refacere legaturi
		temp1->st = temp1->st->st; 
		free(temp1->st);
	}
	else
		//nu avem frunza la st
		if (temp1->st == NULL) {
			//mutare inf din nodul din st, in nodul curent
			temp1->info = temp1->dr->info; 
			temp1->dr = temp1->dr->dr; 
			free (temp1->dr);


			//cazul 3

		}

		else {

			//mergem la frunza din dreapta
			temp = temp1->dr; 
			temp2 = temp1;
			//caut frunza la stanga
			while (temp->st != NULL)
			{
				temp2 = temp;
				temp = temp->st;
			}

			temp1->info = temp->info; 
			temp2->st = NULL; 
			free(temp);
		}//gata 

	return 0;
}



void stergereArbore(Nod* & rad) {
	if (rad) {
		stergereArbore(rad->st);
		stergereArbore(rad->dr);
		free(rad->info.nume);
		free(rad);
		rad = NULL;
	}
}


void main() {
	Nod *rad = NULL;
	int x;
	FILE* f = fopen("TariEuropene.txt", "r");
	while (!feof(f)) {
		Tara t = citesteDinFisier(f);
		rad = inserareTaraInArbore(rad, t);
	}
	fclose(f);

	/*afisareInordine(rad);

	printf("\n\n");*/
	afisareTara(cautareTaraDupaId(rad, 2));
	x = cautareNrLocTari(rad, 200);
	printf("%d", x);
	int y;
		y = StergeNod(rad, 2);
		printf("%d", y);
		stergereArbore(rad);
}