#include<stdlib.h>
#include<stdio.h>
#include<string.h>


struct Tara {
	int id;
	char* nume;
	int nrLoc;
};

struct nod {
	Tara info;
	nod*st;
	nod* dr;
};

Tara cautareTaraDupaId(nod* rad, int id) {
	if (rad) {
		if (rad->info.id == id) {
			return rad->info;
		}
		else if (rad->info.id > id) {
			return cautareTaraDupaId(rad->st, id);
		}
		else {
			return cautareTaraDupaId(rad->dr, id);
		}
	}
	else {
		throw "Nu exista tara cu id-ul cautat";
	}
}
int returneazaTariIDdat(nod* rad, int id)
{
	printf("Te rog sa imi spui un id:");
	printf("\n");
	scanf("%d", id);
	if (id) {
		if (rad->info.id == id) {


			return rad->info.id;
		}

		else if (rad->info.id != id)
		{
			throw "Te rog sa incerci alt ID";
		}
		else
		{
			throw "Nu am arbore!";
		}
	}
}


void plan1(struct nod **Arbore, struct nod * nou, struct nod *sel)
{
	
	struct nod* frunza;
	frunza = (nod*)malloc(sizeof(nod));
	printf("Nid sters=%d\n", nou->info);
	if (nou->dr == NULL && nou->st == NULL)
		frunza = NULL;
	else if (nou->dr == NULL)
		frunza = nou->st;
	else
		frunza = nou->dr;
	if (sel) {
		if (sel->dr == nou)
			sel->dr = frunza;
		else
			sel->st =frunza;
	}
	else
		*(Arbore) = frunza;//fortez cast la nod* pt Arbore care e nod**
}
void plan2(struct nod** Arbore, struct nod *nou, struct nod *sel )
{
	struct nod *suc, *rad_suc;
	suc = (struct nod*)malloc(sizeof(nod));
	rad_suc = (struct nod*)malloc(sizeof(nod));
	printf("nod sters=%d\n", nou->info);
	suc = nou->dr;
	rad_suc = nou;
	while (suc->st != NULL)//daca exista frunza la stanga de succesor
	{
		rad_suc = suc;// rad suc devine suc (merg un nivel)
		suc = suc->st; //suc devine frunza la stanga de suc
	}
	plan1(Arbore, suc, rad_suc);
	if (sel != NULL)//am sel
	{
		if (nou == sel->dr) //frunzele sel devin curent/nou
		sel->dr = suc;
		else
			sel->st = suc;
	}
	else
		(*Arbore) = suc;
	suc->st = nou->st;
	suc->dr = nou->dr;
	//n-am  suc, el devine nou
}

Tara cautareTaraDupaNume(nod* rad, int id) {
	if (rad) {
		if (rad->info.id == id) {
			return rad->info;
		}
		else if (rad->info.id > id) {
			return cautareTaraDupaNume(rad->st, id);
		}
		else {
			return cautareTaraDupaNume(rad->dr, id);
		}
	}
	else {
		throw "Nu exista tara cu id-ul cautat";
	}
}

void stergeNodId(struct nod** Arbore, int id)
{

	struct nod* nou;
	struct nod* sel;
	struct nod* temp;

	nou = (nod*)malloc(sizeof(nod));
	sel = (nod*)malloc(sizeof(nod));
	temp = (nod*)malloc(sizeof(nod));
	temp = *Arbore;

	if (!temp)
		return;
	sel->info = cautareTaraDupaId(temp, 22);

		throw "nu a fost selectat nodul, nu exista! \n";
        if (nou->dr != NULL && nou->st != NULL) //adica, daca am ramuri/frunze > si < decat rad
		plan2(Arbore, nou, sel);

	else
		plan1(Arbore, nou, sel);
